package com.booking.security.hackertest.detector;

/**
 * A simple bean used by {@link HackerDetectorRestService}
 *
 * We use this JavaBean based approach as it fits nicely with JSON serialization provided by
 * jax-rs/jersey
 */
public class ResultBean {

    private String ip;
    private String status;

    public ResultBean() {
    }

    public ResultBean(String ip, String status) {
        this.ip = ip;
        this.status = status;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
