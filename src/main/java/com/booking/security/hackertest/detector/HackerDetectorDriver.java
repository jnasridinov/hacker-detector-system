package com.booking.security.hackertest.detector;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.time.Instant;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class HackerDetectorDriver {

    public static void main(String [] args) throws Exception {
        final String bootstrapServers = args.length > 0 ? args[0] : "localhost:9092";

        final Producer<String, String> producer = createProducer(bootstrapServers);

        try {
            Random random = new Random();

            while (true) {
                long epoch = Instant.now().getEpochSecond();

                String ip = "192.168.2." + random.nextInt(255);
                String name = "Jamshed.Nasridinov";
                String status = random.nextInt(2) == 0 ? "FAILURE" : "SUCCESS";

                String value = String.format("%d,%s,%s,%s", epoch, ip, name, status);

                System.out.println("activity log: " + value);

                producer.send(new ProducerRecord<>(HackerDetectorSystem.ACTIVITY_LOG_TOPIC,
                        null, value));
                Thread.sleep(500L);
            }
        } finally {
            try {
                producer.flush();
                producer.close();
            } catch (Exception e) {
                // ignored
            }
        }
    }

    private static Producer<String, String> createProducer(String bootstrapServers) {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaExampleProducer");
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        props.put(ProducerConfig.RETRIES_CONFIG, 0);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class.getName());

        return new KafkaProducer<>(props, new StringSerializer(), new StringSerializer());
    }

    static void runProducerAsync(final int sendMessageCount, String bootstrapServers) throws InterruptedException {
        final Producer<String, String> producer = createProducer(bootstrapServers);
        long time = System.currentTimeMillis();
        final CountDownLatch countDownLatch = new CountDownLatch(sendMessageCount);

        try {
            Random random = new Random();

            for (long index = time; index < time + sendMessageCount; index++) {
                long epoch = Instant.now().getEpochSecond();
                String ip = "192.168.2." + random.nextInt(255);
                String name = "Jamshed.Nasridinov";
                String status = "FAILURE";

                String value = String.format("%d,%s,%s,%s", epoch, ip, name, status);

                final ProducerRecord<String, String> record =
                        new ProducerRecord<>(HackerDetectorSystem.ACTIVITY_LOG_TOPIC, null, value);

                producer.send(record, (metadata, exception) -> {
                    long elapsedTime = System.currentTimeMillis() - time;
                    if (metadata != null) {
                        System.out.printf("sent record(key=%s value=%s) meta(partition=%d, offset=%d) time=%d\n",
                                record.key(), record.value(), metadata.partition(),
                                metadata.offset(), elapsedTime);
                    } else {
                        exception.printStackTrace();
                    }
                    countDownLatch.countDown();
                });
            }
            countDownLatch.await(25, TimeUnit.SECONDS);
        } finally {
            producer.flush();
            producer.close();
        }
    }

}
