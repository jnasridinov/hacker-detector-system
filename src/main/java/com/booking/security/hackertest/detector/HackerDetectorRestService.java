/**
 * Copyright 2016 Confluent Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.booking.security.hackertest.detector;

import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.state.*;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 *  A simple REST proxy that runs embedded in the {@link HackerDetectorSystem}.
 */
@Path("hacker-detector")
public class HackerDetectorRestService {

    private final KafkaStreams streams;
    private final MetadataService metadataService;
    private final HostInfo hostInfo;

    private final Client client = ClientBuilder.newBuilder().register(JacksonFeature.class).build();

    private Server jettyServer;

    HackerDetectorRestService(final KafkaStreams streams, final HostInfo hostInfo) {
        this.streams = streams;
        this.metadataService = new MetadataService(streams);
        this.hostInfo = hostInfo;
    }

    /**
     * Get the metadata for all of the instances of this Kafka Streams application
     * @return List of {@link HostStoreInfo}
     */
    @GET()
    @Path("/instances")
    @Produces(MediaType.APPLICATION_JSON)
    public List<HostStoreInfo> streamsMetadata() {
        return metadataService.streamsMetadata();
    }

    /**
     * Get the metadata for all instances of this Kafka Streams application that currently
     * has the provided store.
     * @param store   The store to locate
     * @return  List of {@link HostStoreInfo}
     */
    @GET()
    @Path("/instances/{storeName}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<HostStoreInfo> streamsMetadataForStore(@PathParam("storeName") String store) {
        return metadataService.streamsMetadataForStore(store);
    }

    /**
     * Find the metadata for the instance of this Kafka Streams Application that has the given
     * store and would have the given key if it exists.
     * @param store   Store to find
     * @param key     The key to find
     * @return {@link HostStoreInfo}
     */
    @GET()
    @Path("/instance/{storeName}/{key}")
    @Produces(MediaType.APPLICATION_JSON)
    public HostStoreInfo streamsMetadataForStoreAndKey(@PathParam("storeName") String store,
                                                       @PathParam("key") String key) {
        return metadataService.streamsMetadataForStoreAndKey(store, key, new StringSerializer());
    }

    /**
     * Get all of the key-value pairs available in a store
     * @param storeName   store to query
     * @return A List of {@link KeyValueBean}s representing all of the key-values in the provided
     * store
     */
    @GET()
    @Path("/keyvalues/{storeName}/all")
    @Produces(MediaType.APPLICATION_JSON)
    public List<KeyValueBean> allForStore(@PathParam("storeName") final String storeName) throws InterruptedException {
        return rangeForKeyValueStore(storeName, ReadOnlyKeyValueStore::all);
    }

    /**
     * Performs a range query on a KeyValue Store and converts the results into a List of
     * {@link KeyValueBean}
     * @param storeName       The store to query
     * @param rangeFunction   The range query to run, i.e., all, from(start, end)
     * @return  List of {@link KeyValueBean}
     */
    private List<KeyValueBean> rangeForKeyValueStore(final String storeName,
                                                     final Function<ReadOnlyKeyValueStore<String, Long>,
                                                             KeyValueIterator<String, Long>> rangeFunction) {
        // Get the KeyValue Store
        final ReadOnlyKeyValueStore<String, Long> store = streams.store(storeName, QueryableStoreTypes.keyValueStore());
        if (store == null) {
            throw new NotFoundException();
        }

        final List<KeyValueBean> results = new ArrayList<>();
        // Apply the function, i.e., query the store
        final KeyValueIterator<String, Long> range = rangeFunction.apply(store);

        // Convert the results
        while (range.hasNext()) {
            final KeyValue<String, Long> next = range.next();
            results.add(new KeyValueBean(next.key, next.value));
        }

        return results;
    }

    /**
     * Query a window store for key-value pairs representing the value for a provided key within a
     * range of windows. Iterate through the range and aggregate total amount of occurrences within last 5 minutes.
     * @param key         key to look for
     * @return {@link ResultBean}
     */
    @GET()
    @Path("/windowed/{key}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResultBean windowedByKey(@PathParam("key") final String key) {

        HostStoreInfo hostStoreInfo = metadataService.streamsMetadataForStoreAndKey(
                HackerDetectorSystem.WINDOWED_ACTIVITY_LOG_BY_IP_STORE, key, new StringSerializer());

        System.out.println("hostStoreInfo = " + hostStoreInfo.getHost() + ": " + hostStoreInfo.getPort() + ", " + hostStoreInfo.getStoreNames());

        if (!isLocalHost(hostStoreInfo)) {
            return requestRemote(hostStoreInfo, String.format("hacker-detector/windowed/%s", key));
        }

        // Lookup the WindowStore with the provided storeName
        final ReadOnlyWindowStore<String, Long> store =
                streams.store(HackerDetectorSystem.WINDOWED_ACTIVITY_LOG_BY_IP_STORE,
                        QueryableStoreTypes.<String, Long>windowStore());

        if (store == null) {
            throw new NotFoundException();
        }

        long timeTo = System.currentTimeMillis();
        long timeFrom = timeTo - HackerDetectorSystem.WINDOW_IN_MILLIS;

        // fetch the window results for the given key and time range
        final WindowStoreIterator<Long> results = store.fetch(key, timeFrom, timeTo);

        long attempts = 0;

        while (results.hasNext()) {
            final KeyValue<Long, Long> next = results.next();

            System.out.println(key + "@" + next.key + " = " + next.value);

            attempts += next.value;
        }

        return new ResultBean(key, isSuspicious(attempts));
    }

    /**
     * Determine the status depending on number of attempts
     * @param attempts number of attempts
     * @return suspicious if greater or equal to attempts threshold
     */
    private String isSuspicious(long attempts) {
        return attempts >= HackerDetectorSystem.ATTEMPTS_THRESHOLD ? HackerDetectorSystem.SUSPICIOUS_IP : "";
    }

    /**
     * Request the remote host
     * @param host  remote host
     * @param path  remote path
     * @return {@link KeyValueBean}
     */
    private ResultBean requestRemote(HostStoreInfo host, String path) {
        String uri = String.format("http://%s:%d/%s", host.getHost(), host.getPort(), path);

        System.out.println("request remote uri = " + uri);

        return client.target(uri)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(new GenericType<ResultBean>() {});
    }

    /**
     * Match local host and port to the given one
     * @param host  host to compare with
     * @return  the result of matching
     */
    private boolean isLocalHost(final HostStoreInfo host) {
        return host.getHost().equals(hostInfo.host()) &&
                host.getPort() == hostInfo.port();
    }

    /**
     * Start an embedded Jetty Server on the given port
     * @param port    port to run the Server on
     * @throws Exception
     */
    void start(final int port) throws Exception {
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");

        jettyServer = new Server(port);
        jettyServer.setHandler(context);

        ResourceConfig rc = new ResourceConfig();
        rc.register(this);
        rc.register(JacksonFeature.class);

        ServletContainer sc = new ServletContainer(rc);
        ServletHolder holder = new ServletHolder(sc);
        context.addServlet(holder, "/*");

        jettyServer.start();
    }

    /**
     * Stop the Jetty Server
     * @throws Exception
     */
    void stop() throws Exception {
        if (jettyServer != null) {
            jettyServer.stop();
        }
    }

}

