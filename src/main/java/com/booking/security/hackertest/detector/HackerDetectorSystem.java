package com.booking.security.hackertest.detector;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.HostInfo;
import org.apache.kafka.streams.state.WindowStore;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * Hacker detection system
 * Security is a real concern for us at Booking.com as we are targeted by cybercriminals every day.
 * To protect our services, you need a system which identifies hack attempts based on the activity log.
 * If failed attempts are coming from a single IP address 5 or more times within a 5-minute period means that
 * those IP addresses suspicious.
 *
 * Note: Before running this example you must
 * 1) create the source topic
 * <pre>
 * {
 * $ bin/kafka-topics.sh --create \
 * --zookeeper localhost:2181 \
 * --replication-factor 1 \
 * --partitions 3 \
 * --topic activity-log-input
 * }
 * </pre>
 *
 * HOW TO RUN
 *
 * 1) Start Zookeeper
 * <pre>
 * {@code
 * $ bin/zookeeper-server-start.sh config/zookeeper.properties
 * }
 * </pre>
 *
 * 2) Start Kafka server
 * <pre>
 * {@code
 * $ bin/kafka-server-start.sh config/server.properties
 * }
 * </pre>
 *
 * 3) Start two instances of this application either in your IDE or on the command line on port 7070
 *
 * Here, `7070` sets the port for the REST endpoint that will be used by this application instance.
 *
 * Then, run the second instance of this application (on port 7071):
 *
 * 4) Write some input data to the source topics (e.g. via {@link HackerDetectorDriver}).
 *
 * 5) Use your browser to hit the REST endpoint of the app instance you started in step 3 to query
 * the state managed by this application.  Note: If you are running multiple app instances, you can
 * query them arbitrarily -- if an app instance cannot satisfy a query itself, it will fetch the
 * results from the other instances.
 *
 * For example:
 *
 * <pre>
 * {@code
 * # Determines if the given IP is suspicious
 * http://localhost:7070/hacker-detector/windowed/187.218.83.135
 *
 * # List app instances that currently manage (parts of) state store activity-log-by-ip
 * http://localhost:7070/hacker-detector/instances/activity-log-by-ip
 *
 * # Get all key-value records from the "activity-log-by-ip" state store hosted on a the instance running
 * # localhost:7070
 * http://localhost:7070/hacker-detector/keyvalues/activity-log-by-ip/all
 *
 * # Find the app instance that contains key "187.218.83.135" (if it exists) for the state store "activity-log-by-ip"
 * http://localhost:7070/state/instance/activity-log-by-ip/187.218.83.135
 *
 * # Get the latest value for key "187.218.83.135" in state store "activity-log-by-ip"
 * http://localhost:7070/hacker-detector/keyvalue/activity-log-by-ip/187.218.83.135
 * }
 * </pre>
 *
 * Note: that the REST functionality is NOT part of Kafka Streams or its API. For demonstration
 * purposes of this example application, we decided to go with a simple, custom-built REST API that
 * uses the Interactive Queries API of Kafka Streams behind the scenes to expose the state stores of
 * this application via REST.
 *
 * 6) Once you're done with your experiments, you can stop this example via `Ctrl-C`.  If needed,
 * also stop the Kafka broker (`Ctrl-C`), and only then stop the ZooKeeper instance (`Ctrl-C`).
 *
 * If you like you can run multiple instances of this example by passing in a different port. You
 * can then experiment with seeing how keys map to different instances etc.
 */

public class HackerDetectorSystem {

    // 1507365137,187.218.83.136,John.Smith,SUCCESS
    // 1507365137,187.218.83.136,John.Smith,FAILURE

    private static final String STATUS_FAILURE = "FAILURE";
    private static final String STATUS_UNKNOWN = "UNKNOWN";

    private static final String APPLICATION_ID_CONFIG = "hacker-system-detector";
    private static final String ACTIVITY_LOG_BY_IP_STORE = "activity-log-by-ip";
    private static final String DEFAULT_REST_ENDPOINT_HOSTNAME = "localhost";
    private static final String DEFAULT_BOOTSTRAP_SERVERS = "localhost:9092";

    static final String ACTIVITY_LOG_TOPIC = "activity-log-input";
    static final String WINDOWED_ACTIVITY_LOG_BY_IP_STORE = "windowed-activity-log-by-ip";
    static final String SUSPICIOUS_IP = "SUSPICIOUS";

    private static final int WINDOW_IN_MINUTES = 5;

    static final int ATTEMPTS_THRESHOLD = 5;

    static final long WINDOW_IN_MILLIS = WINDOW_IN_MINUTES * 60 * 1000;

    public static void main(String[] args) throws Exception {
        if (args.length == 0 || args.length > 3) {
            throw new IllegalArgumentException("usage: ... <portForRestEndpoint> " +
                    "[<bootstrap.servers> (optional, default: " + DEFAULT_BOOTSTRAP_SERVERS + ")] " +
                    "[<hostnameForRestEndPoint> (optional, default: " + DEFAULT_REST_ENDPOINT_HOSTNAME + ")]");
        }

        final int restEndpointPort = Integer.valueOf(args[0]);
        final String bootstrapServers = args.length > 1 ? args[1] : "localhost:9092";
        final String restEndpointHostname = args.length > 2 ? args[2] : DEFAULT_REST_ENDPOINT_HOSTNAME;

        final HostInfo restEndpoint = new HostInfo(restEndpointHostname, restEndpointPort);

        System.out.println("Connecting to Kafka cluster via bootstrap servers " + bootstrapServers);
        System.out.println("REST endpoint at http://" + restEndpointHostname + ":" + restEndpointPort);

        final KafkaStreams streams = createStreams(bootstrapServers,
                restEndpointPort
        );

        streams.cleanUp();

        // Now that we have finished the definition of the processing topology we can actually run
        // it via `start()`.  The Streams application as a whole can be launched just like any
        // normal Java application that has a `main()` method.
        streams.start();

        final HackerDetectorRestService restService = startRestProxy(streams, restEndpoint, restEndpointPort);

        // Add shutdown hook to respond to SIGTERM and gracefully close Kafka Streams
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                restService.stop();
                streams.close();
            } catch (Exception e) {
                // ignored
            }
        }));
    }

    static KafkaStreams createStreams(final String bootstrapServers,
                                      final int applicationServerPort) {
        final Properties props = new Properties();

        // Give the Streams application a unique name.  The name must be unique in the Kafka cluster
        // against which the application is run.
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, APPLICATION_ID_CONFIG);

        // Where to find Kafka broker(s).
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);

        // Provide the details of our embedded http service that we'll use to connect to this streams
        // instance and discover locations of stores.
        props.put(StreamsConfig.APPLICATION_SERVER_CONFIG, "localhost:" + applicationServerPort);

        try {
            final File file = Files.createTempDirectory(new File("/tmp").toPath(), APPLICATION_ID_CONFIG).toFile();
            props.put(StreamsConfig.STATE_DIR_CONFIG, file.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Set to earliest so we don't miss any data that arrived in the topics before the process started
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        // Set the commit interval to 500ms so that any changes are flushed frequently
        props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 500);

        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);

        StreamsBuilder builder = new StreamsBuilder();

        KStream<String, String> source = builder.stream(ACTIVITY_LOG_TOPIC);

        final KGroupedStream<String, String> groupedByWord = source
                .filter((key, value) -> isFailureAttempt(getStatus(value)))
                .selectKey(new KeyValueMapper<String, String, String>() {
                    @Override
                    public String apply(String key, String value) {
                        System.out.println("flatMapValues value = " + value);

                        return getIPAddress(value);
                    }
                }).
                groupByKey();

        // Create a State Store for with the all time IP count
        groupedByWord.count(
                Materialized.as(ACTIVITY_LOG_BY_IP_STORE));

        // Create a Windowed State Store that contains the IP count for give window
        groupedByWord
                .windowedBy(TimeWindows.of(TimeUnit.MINUTES.toMinutes(WINDOW_IN_MINUTES)))
                .count(Materialized.<String, Long, WindowStore<Bytes, byte[]>>as(WINDOWED_ACTIVITY_LOG_BY_IP_STORE)
                        .withKeySerde(Serdes.String()));

        /*

        Alternatively we can put suspicious IPs to arbitrary output topic and read from it

        KTable<Windowed<String>, Long> counts = source
                .filter((key, value) -> isFailureAttempt(getStatus(value)))
                .groupByKey()
                .windowedBy(TimeWindows.of(TimeUnit.SECONDS.toMillis(10)))
                .count(Materialized.<String, Long, WindowStore<Bytes, byte[]>>as(ACTIVITY_LOG_BY_IP_STORE)
                        .withValueSerde(Serdes.Long())).
                .filter(((key, value) -> value >= WINDOW_IN_MINUTES));

        counts.toStream((key, value) -> key.toString())
            .to("activity-log-output", Produced.with(Serdes.String(), Serdes.Long()));

         */

        return new KafkaStreams(builder.build(), props);
    }

    static HackerDetectorRestService startRestProxy(final KafkaStreams streams, final HostInfo hostInfo, final int port)
            throws Exception {
        final HackerDetectorRestService
                hackerDetectorRestService = new HackerDetectorRestService(streams, hostInfo);
        hackerDetectorRestService.start(port);

        return hackerDetectorRestService;
    }

    private static String getStatus(String line) {
        String parseLine[] = line.split(",");

        if (parseLine.length < 4) {
            return STATUS_UNKNOWN;
        }

        return parseLine[3];
    }

    private static String getIPAddress(String line) {
        String parseLine[] = line.split(",");

        return parseLine[1];
    }

    private static boolean isFailureAttempt(String status) {
        return status.equals(STATUS_FAILURE);
    }

}
