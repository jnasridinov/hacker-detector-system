#OVERVIEW

Hacker detection system Security is a real concern for us at Booking.com as we are targeted by cybercriminals every day.
To protect our services, you need a system which identifies hack attempts based on the activity log.
If failed attempts are coming from a single IP address 5 or more times within a 5-minute period means that those
IP addresses suspicious.

Note: Before running this example you must

1) create the source topic
$ bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 3 --topic activity-log-input

#HOW TO RUN

1) Start Zookeeper
$ bin/zookeeper-server-start.sh config/zookeeper.properties

2) Start Kafka server
$ bin/kafka-server-start.sh config/server.properties

3) Start two instances of this application either in your IDE or on the command line on port 7070
Here, 7070 sets the port for the REST endpoint that will be used by this application instance.
Then, run the second instance of this application (on port 7071):

4) Write some input data to the source topics (e.g. via { HackerDetectorDriver}).

5) Use your browser to hit the REST endpoint of the app instance you started in step 3 to query the state managed
by this application. Note: If you are running multiple app instances, you can query them arbitrarily -- if an app
instance cannot satisfy a query itself, it will fetch the results from the other instances.

#For example:

Determines if the given IP is suspicious
http://localhost:7070/hacker-detector/windowed/187.218.83.135

List app instances that currently manage (parts of) state store activity-log-by-ip
http://localhost:7070/hacker-detector/instances/activity-log-by-ip

Get all key-value records from the "activity-log-by-ip" state store hosted on a the instance running
localhost:7070 http://localhost:7070/hacker-detector/keyvalues/activity-log-by-ip/all

Find the app instance that contains key "187.218.83.135" (if it exists) for the state store "activity-log-by-ip"
http://localhost:7070/state/instance/activity-log-by-ip/187.218.83.135

Get the latest value for key "187.218.83.135" in state store "activity-log-by-ip"
http://localhost:7070/hacker-detector/keyvalue/activity-log-by-ip/187.218.83.135 } </pre>

Note: that the REST functionality is NOT part of Kafka Streams or its API. For demonstration purposes of this example
application, we decided to go with a simple, custom-built REST API that uses the Interactive Queries API of
Kafka Streams behind the scenes to expose the state stores of this application via REST.

6) Once you're done with your experiments, you can stop this example via Ctrl-C. If needed, also stop the Kafka
broker (Ctrl-C), and only then stop the ZooKeeper instance (Ctrl-C). If you like you can run multiple instances of
this example by passing in a different port. You can then experiment with seeing how keys map to different instances etc.